php-application-toolkit
=======================

Allrounder, very abstract and extensible framework.

Find / Fork / Help us
---------------------

This framework is on several different platforms available.
Feel free to fork them and make your own changes.
We will be thankful for every pull request or patch-file that comes back to us :)

* [on Bitbucket.org](http://bitbucket.org/rmp/php-application-toolkit/overview)
  ( `http://git@bitbucket.org/rmp/php-application-toolkit.git` )
* [on GitHub.com](http://github.com/sourcerer-mike/php-application-toolkit)
  ( `git://github.com/sourcerer-mike/php-application-toolkit.git` )
* [on Google Code](http://code.google.com/p/php-application-toolkit/)
  ( `https://code.google.com/p/php-application-toolkit/` )
* [on SourceForge](http://sourceforge.net/p/php-app-toolkit)
  ( `ssh://your-username@git.code.sf.net/p/php-app-toolkit/code` )